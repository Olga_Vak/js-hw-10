// Написать реализацию кнопки "Показать пароль".
//
//     Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const eyeShown = document.getElementById('eyeShown');
const eyeHide = document.getElementById('eyeHide');


function changeOfInputState() {
    if (this.classList.contains('fa-eye')) {
        this.previousElementSibling.removeAttribute('type');
        this.previousElementSibling.setAttribute('type', 'text');
        this.classList.remove('fa-eye');
        this.classList.add('fa-eye-slash');
    } else if (this.classList.contains('fa-eye-slash')) {
        this.previousElementSibling.removeAttribute('type');
        this.previousElementSibling.setAttribute('type', 'password');
        this.classList.remove('fa-eye-slash');
        this.classList.add('fa-eye');
    }
}

eyeShown.onclick = changeOfInputState;
eyeHide.onclick = changeOfInputState;

const submitButton = document.getElementById('submit-btn');
submitButton.addEventListener('click',function (event) {
    event.preventDefault()

const textOfPassword = document.querySelector('.enter-password').value;
const textOfValidPassword = document.querySelector('.repeat-password').value;


    if (textOfPassword === textOfValidPassword){
        alert('You are welcome');
    } else {
        let warning = document.createElement('p');
        warning.innerText = 'Нужно ввести одинаковые значения!';
        warning.style.color = 'red';
        submitButton.insertAdjacentElement("beforebegin", warning);
        const inputPassword = document.querySelector('.enter-password');
        inputPassword.addEventListener('focus',function () {
            warning.remove();

        });
    }

});

